var settings = {
  async: true,
  crossDomain: true,
  url: "https://andreemalerva.com/pruebaSysOp/laravel/back-test/public/api/usuarios",
  method: "GET",
  data: {},
};

$.ajax(settings).done(function (response) {
  console.log(response.data);
  function iterate(item) {
    $("#datos").append(
      "<tr>" +
        "<td class='idInfo info"+item.id+"' id='ID"+item.id+"'>" + item.id +"</td>" +
        "<td class='idUser user"+item.id+"' id='idUser"+item.id+"'>" + item.usuario +"</td>" +
        "<td class='idNombre nombre"+item.id+"' id='idNombre"+item.id+"'>" + item.nombre + "</td>" +
        "<td class='idApellido ape"+item.id+"' id='idApellido"+item.id+"'>" + item.apellido +"</td>" +
        "<td class='idCorreo correo"+item.id+"' id='idCorreo"+item.id+"'>" + item.correo +"</td>" +
        "<td class='idTelefono tel"+item.id+"' id='telefono"+item.id+"'>" + item.telefono +"</td>" +
        "<td class='idPassword pass"+item.id+"' id='pass"+item.id+"'>" + item.password +"</td>" +
        "<td><button type='button' class='dataEdit me-3 btn btn-warning' id=" +item.id + ">Editar</button>" +
        "<button type='submit' class='dataDelete btn btn-danger' id=" +item.id + ">Eliminar</button></td></tr>"
    );
    dataEdit();
    deleteDatos();
  }

  Array.prototype.forEach.call(response.data, iterate);
});

const createdUser = document.getElementById("createdUser");
createdUser.addEventListener("click", function () {
  document.getElementById('formForUser').reset();
  document.getElementById("titleModal").textContent = "Crear Usuario";
  document.getElementById("buttonInformation").classList.add("btn-success");
  document.getElementById("buttonInformation").textContent = "Crear";
  document.getElementById("msg").style.display = "none";
  document.getElementById('divId').style.display = "none";
  let modalForm = bootstrap.Modal.getOrCreateInstance(document.getElementById("modalForm"));
  modalForm.show();

  document
    .getElementById("buttonInformation")
    .addEventListener("click", function () {
        createdData();
    });
});


function dataEdit() {
    document.getElementById('divId').style.display = "flex";
    var dataEdit = document.querySelectorAll(".dataEdit");
    dataEdit.forEach((element) => {
        element.addEventListener("click", function () {
        
            const datoId = element.id;
            document.getElementById("titleModal").textContent = "Actualizar Usuario";
            document.getElementById("msg").style.display = "none";
            document.getElementById("buttonInformation").textContent = "Actualizar";
            let modalForm = bootstrap.Modal.getOrCreateInstance(document.getElementById("modalForm"));
            modalForm.show();

            var idInfo = document.querySelectorAll(".idInfo");
            var idUser = document.querySelectorAll(".idUser");
            var idNombre = document.querySelectorAll(".idNombre");
            var idApellido = document.querySelectorAll(".idApellido");
            var idCorreo = document.querySelectorAll(".idCorreo");
            var idTelefono = document.querySelectorAll(".idTelefono");
            var idPassword = document.querySelectorAll(".idPassword");
            
            idInfo.forEach((itemsInfo) => {
                if (itemsInfo.id = datoId) {
                    var IDInputID = document.querySelector('.info'+itemsInfo.id).textContent;
                    document.getElementById("idInput").value = datoId;
                    document.getElementById("idInput").style.backgroundColor = '#9A9494';
                    document.getElementById("buttonInformation").classList.add("btn-warning");
                    document.getElementById("buttonInformation").addEventListener("click", function () {
                        const id = document.getElementById("idInput").value = itemsInfo.id;
                        var usuario = document.getElementById("usuario").value;
                        var nombre = document.getElementById("nombre").value;
                        var apellido = document.getElementById("apellido").value;
                        var correo = document.getElementById("correo").value;
                        var telefono = document.getElementById("telefono").value;
                        var password = document.getElementById("password").value;
                            updateData(id, usuario,nombre, apellido, correo, telefono, password);
                        });
                } 

            })

            idUser.forEach((itemsUsers) => {
                if (itemsUsers.id = datoId) {
                    var IDInput = document.querySelector('.user'+itemsUsers.id).textContent;
                    document.getElementById("usuario").value = IDInput;
                } 
            })

            idNombre.forEach((itemsNombre) => {
                if (itemsNombre.id = datoId) {
                    var IDInputNombre = document.querySelector('.nombre'+itemsNombre.id).textContent;
                    document.getElementById("nombre").value = IDInputNombre;
                } 
            })

            idApellido.forEach((itemsApellido) => {
                if (itemsApellido.id = datoId) {
                    var IDInputApellido = document.querySelector('.ape'+itemsApellido.id).textContent;
                    document.getElementById("apellido").value = IDInputApellido;
                } 
            })

            idCorreo.forEach((itemsCorreo) => {
                if (itemsCorreo.id = datoId) {
                    var IDInputCorreo = document.querySelector('.correo'+itemsCorreo.id).textContent;
                    document.getElementById("correo").value = IDInputCorreo;
                } 
            })

            idTelefono.forEach((itemsTelefono) => {
                if (itemsTelefono.id = datoId) {
                    var IDInputTelefono = document.querySelector('.tel'+itemsTelefono.id).textContent;
                    document.getElementById("telefono").value = IDInputTelefono;
                } 
            })

            idPassword.forEach((itemsPass) => {
                if (itemsPass.id = datoId) {
                    var IDInputPass = document.querySelector('.pass'+itemsPass.id).textContent;
                    document.getElementById("password").value = IDInputPass;
                } 
            })  
            
        });
    });
}

function updateData(id, usuario,nombre, apellido, correo, telefono, password) {
    console.log('soy id', id, 'soy usuario',usuario,'soy nombre',nombre, 'soy apellido',apellido, 'soy correo',correo, 'soy telefono',telefono, 'soy password',password);
  
    let informationObjs = {
      id: id,
      usuario: usuario,
      nombre: nombre,
      apellido: apellido,
      correo: correo,
      telefono: telefono,
      password: password,
    };
  
    var settings = {
        url: "https://andreemalerva.com/pruebaSysOp/laravel/back-test/public/api/usuarios/"+id+'?',
        type: 'PUT',
        dataType: 'json',
        data: informationObjs,
    };
  
    $.ajax(settings)
    .done(function () {
        console.log('SUCCESS');
        document.getElementById("titleModal").textContent = "Información";
        document.getElementById("msg").style.display = "block";
        document.getElementById("msg").textContent = "El usuario ha sido Actualizado";
        let modalInformationShow = bootstrap.Modal.getOrCreateInstance(
            document.getElementById("modalForm")
          );
          modalInformationShow.show();
          document.getElementById("formInputs").style.display = "none";
          document.getElementById("buttonInformation").style.display = "none";
          
        const modalInformation = document.getElementById("modalForm");
        modalInformation.addEventListener("hidden.bs.modal", (event) => {
            console.log('aqui');
            location.reload();
        });
    }).fail(function (response) {
        console.log('FAIL', response);
        document.getElementById("titleModal").textContent = "Información";
          document.getElementById("msg").textContent =
            "Ha fallado la eliminación";
          document.getElementById("formInputs").style.display = "none";
          document.getElementById("buttonInformation").style.display = "none";

          let modalInformationShow = bootstrap.Modal.getOrCreateInstance(
            document.getElementById("modalForm")
          );
          modalInformationShow.show();

          const modalInformation = document.getElementById("modalForm");
          modalInformation.addEventListener("hidden.bs.modal", (event) => {
            location.reload();
          });
    }).always(function (msg) {
        console.log('ALWAYS', msg);
    });
}

function createdData() {
  const id = document.getElementById("idInput").value = "";
  const usuario = document.getElementById("usuario").value;
  const nombre = document.getElementById("nombre").value;
  const apellido = document.getElementById("apellido").value;
  const correo = document.getElementById("correo").value;
  const telefono = document.getElementById("telefono").value;
  const password = document.getElementById("password").value;

  let informationObjs = {
    id: id,
    usuario: usuario,
    nombre: nombre,
    apellido: apellido,
    correo: correo,
    telefono: telefono,
    password: password,
  };

  var settings = {
    async: true,
    crossDomain: true,
    url: "https://andreemalerva.com/pruebaSysOp/laravel/back-test/public/api/usuarios",
    method: "POST",
    data: informationObjs,
  };

  $.ajax(settings)
    .done(function (response) {
      console.log(response);
      location.reload();
    })
    .fail(function () {
      console.log("error");
    });
}

function deleteDatos() {
  var dataDelete = document.querySelectorAll(".dataDelete");
  dataDelete.forEach((item) => {
    item.addEventListener("click", function () {
      const datoId = item.id;
      var settings = {
        async: true,
        crossDomain: true,
        url:
          "https://andreemalerva.com/pruebaSysOp/laravel/back-test/public/api/usuarios/" +
          datoId,
        method: "DELETE",
        data: {},
      };

      $.ajax(settings)
        .done(function (response) {
          console.log(response);
          document.getElementById("titleModal").textContent = "Información";
          document.getElementById("msg").textContent =
            "El usuario ha sido Eliminado";
          document.getElementById("formInputs").style.display = "none";
          document.getElementById("buttonInformation").style.display = "none";

          let modalInformationShow = bootstrap.Modal.getOrCreateInstance(
            document.getElementById("modalForm")
          );
          modalInformationShow.show();

          const modalInformation = document.getElementById("modalForm");
          modalInformation.addEventListener("hidden.bs.modal", (event) => {
            location.reload();
          });
        })
        .fail(function () {
          console.log("error");
        });
    });
  });
}
