<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
//use Illuminate\Support\Facades\Artisan;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

/* Route::get('/', function () {
    return view('welcome');
}); */

// returns the home page with all posts
/* Route::get('/', PostController::class .'@index')->name('index');
Route::resource('usuarios', PostController::class); */
/* Route::get('/clear-cache', function () {
    Artisan::call('cache:clear');
    Artisan::call('route:clear');

    return "Cache cleared successfully";
 }); */
// returns the home page with all posts
Route::get('/', PostController::class .'@index')->name('usuarios.index');
// returns the form for adding a post
Route::get('/usuarios/create', PostController::class . '@create')->name('usuarios.create');
// adds a post to the database
Route::post('/usuarios', PostController::class .'@store')->name('usuarios.store');
// returns a page that shows a full post
Route::get('/usuarios/{usuario}', PostController::class .'@show')->name('usuarios.show');
// returns the form for editing a post
Route::get('/usuarios/{usuario}/edit', PostController::class .'@edit')->name('usuarios.edit');
// updates a post
Route::put('/usuarios/{usuario}', PostController::class .'@update')->name('usuarios.update');
// deletes a post
Route::delete('/usuarios/{usuario}', PostController::class .'@destroy')->name('usuarios.destroy');
