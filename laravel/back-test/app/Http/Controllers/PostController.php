<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Usuario;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function index()
    {
        $usuarios = Usuario::paginate();
        return response()->json($usuarios);
        //return view('index', compact('usuarios'));
    }

    public function create()
    {
        $usuario = new usuario;
        $title = __('Crear proyecto');
        $action = route('usuarios.store');
        $buttonText = __('Crear Usuario');
        return view('layouts.form', compact('usuario', 'title', 'action', 'buttonText'));
    }

    public function edit(Usuario $usuario)
    {
        $title = __('Editar proyecto');
        $action = route('usuarios.update', $usuario);
        $buttonText = __('Actualizar usuario');
        $method = 'PUT';
        return view('layouts.form', compact('usuario', 'title', 'action', 'buttonText', 'method'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nombre' => 'required|string|max:50',
            'apellido' => 'required|string|max:50',
            'correo' => 'required|string|max:50',
            'telefono' => 'required|string|max:50',
            'usuario' => 'required|string|max:50',
            'password' => 'required|string|max:50',
        ]);

        Usuario::create([
            'id' => $request->id,
            'nombre' => $request->string('nombre'),
            'apellido' => $request->string('apellido'),
            'correo' => $request->string('correo'),
            'telefono' => $request->string('telefono'),
            'usuario' => $request->string('usuario'),
            'password' => $request->string('password'),
        ]);
        return response()->json(['message' => 'El dato ha sido Creado']);
        //return redirect('/');
    }

    public function update(Request $request, Usuario $usuario)
    {
        $request->validate([
            'nombre' => 'required|unique:usuarios,nombre,' . $usuario->id . '|string|max:100',
            'apellido' => 'required|string|max:1000',
            'correo' => 'required|string|max:50',
            'telefono' => 'required|string|max:50',
            'usuario' => 'required|string|max:50',
            'password' => 'required|string|max:50',
        ]);
        $usuario->update([
            'id' => $request->id,
            'nombre' => $request->string('nombre'),
            'apellido' => $request->string('apellido'),
            'correo' => $request->string('correo'),
            'telefono' => $request->string('telefono'),
            'usuario' => $request->string('usuario'),
            'password' => $request->string('password'),
        ]);

        return response()->json(['message' => 'El dato ha sido Actualizado']);
        //return redirect('/');
    }

    public function destroy(Usuario $usuario)
    {
        $usuario->delete();
        return response()->json(['message' => 'El dato ha sido Eliminado']);
        //return redirect('/');
    }
}
