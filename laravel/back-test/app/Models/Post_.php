<?php
class Post extends CI_Model
{

   public function __construct() {
      parent::__construct();
   }

   public function obtenerProductos(){
      $this->db->select('id, precio, producto, proveedor');
      $this->db->from('productos');
      $this->db->order_by('id, precio', 'asc');
      $consulta = $this->db->get();
      $resultado = $consulta->result();
      return $resultado;
   }

   public function saveInfoModel($data){
      $this->db->insert('productos', $data);
   }

   public function deleteInfoModel($id){
      $this->db->where('id', $id);
      $this->db->delete('productos');
   }

   public function updateInfoModel($id, $data){
         $this->db->where('id', $id);
         $this->db->update('productos', $data);
      }

}
?>

