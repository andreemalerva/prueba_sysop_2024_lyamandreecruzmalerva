<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    public $timestamps = false; 
    protected $fillable = ['id','nombre','apellido', 'correo', 'telefono', 'usuario', 'password']; //<---- Add this line

    use HasFactory;
}
